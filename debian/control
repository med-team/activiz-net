Source: activiz.net
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Corentin Desfarges <corentin.desfarges.dev@gmail.com>,
           Andreas Tille <tille@debian.org>
Section: graphics
Priority: optional
Build-Depends: debhelper (>= 9),
               cmake,
               mummy,
               libkitware-mummy-runtime1.0-cil,
               gccxml,
               libvtk5-dev,
               mono-devel,
               cli-common-dev
Build-Depends-Indep: doxygen-latex,
                     graphviz
Standards-Version: 4.2.1
Vcs-Browser: https://salsa.debian.org/med-team/activiz-net
Vcs-Git: https://salsa.debian.org/med-team/activiz-net.git
Homepage: http://www.kitware.com/products/avdownload.php

Package: libactiviz.net-cil
Architecture: any
Section: cli-mono
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${cli:Depends}
Provides: libactiviz.net${vtk:Version}-cil
Description: Tool for generating C# wrappers around VTK
 ActiViz provides a powerful interface to the Visualization Toolkit (VTK), an
 object-oriented software system encompassing thousands of algorithms that
 transform data into interactive 3D environments. ActiViz, which generates C#
 wrappers around VTK, enables developers to combine the power of VTK with the
 many .NET framework objects for web and database access. Available as source
 code or as a pre-built WinForms Control, ActiViz .NET includes examples, online
 documentation, and supports IntelliSense in the .NET Framework

Package: activiz.net-doc
Architecture: all
Section: doc
Depends: ${misc:Depends},
         doc-base
Suggests: vtk-doc
Description: ActiViz.NET documentation
 ActiViz provides a powerful interface to the Visualization Toolkit (VTK), an
 object-oriented software system encompassing thousands of algorithms that
 transform data into interactive 3D environments.
 .
 This package contains exhaustive HTML documentation for the all the
 documented ActiViz.NET C# classes.

Package: activiz.net-examples
Architecture: all
Depends: ${misc:Depends}
Suggests: vtk-examples
Description: ActiViz.NET examples
 ActiViz provides a powerful interface to the Visualization Toolkit (VTK), an
 object-oriented software system encompassing thousands of algorithms that
 transform data into interactive 3D environments.
 The Visualization Toolkit (VTK) is an object oriented, high level
 library that allows one to easily write C++ programs, Tcl, Python and
 Java scripts that do 3D visualization.
 .
 This package contains examples from the ActiViz.NET source.
